package ar.edu.utn.ba.dds.redis.repository;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import ar.edu.utn.ba.dds.redis.exception.NoExisteAlumnoException;
import ar.edu.utn.ba.dds.redis.model.Alumno;

/**
 * Test de la clase AlumnoRepository
 *
 */
public class AlumnoRepositoryTest {

	@Test
	public void saveOk() throws NoExisteAlumnoException, IOException {
		Alumno expected = new Alumno("Mito", "123456789", "111111-1");
		new AlumnoRepository().save(expected);
		Alumno actual = new AlumnoRepository().get(expected.getLegajo());
		assertEquals(expected, actual);
	}

	// public List<Alumno> getAll() {
	// }
	//
	// public Alumno get(String legajo) throws NoExisteAlumnoException, IOException
	// {
	// }
	//
	// public Alumno getAlumnoFromJson(String alumnoJson) throws IOException {
	// }
	//
	// public String getAlumnoJSON(Alumno alumno) {
	// }
	//
	// public String getAlumnosJSON(List<Alumno> alumnos) {
	// }
}