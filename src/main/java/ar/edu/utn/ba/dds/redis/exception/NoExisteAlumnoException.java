package ar.edu.utn.ba.dds.redis.exception;

/**
 * No existe el alumno que se busca.
 *
 */
public class NoExisteAlumnoException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoExisteAlumnoException() {
		super();
	}

	public NoExisteAlumnoException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public NoExisteAlumnoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public NoExisteAlumnoException(String arg0) {
		super(arg0);
	}

	public NoExisteAlumnoException(Throwable arg0) {
		super(arg0);
	}
}