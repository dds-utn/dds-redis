package ar.edu.utn.ba.dds.redis;

import ar.edu.utn.ba.dds.redis.controller.AlumnoController;
import ar.edu.utn.ba.dds.redis.repository.AlumnoRepository;
import spark.Spark;

/**
 * Inicia, configura las rutas y los puertos del servidor.
 *
 */
public class Application {
	private static final int SERVER_PORT = 8080;

	/**
	 * Main principal de la aplicacion.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		initApplication();
	}

	/**
	 * Inicializa los objetos de la aplicacion.
	 */
	private static void initApplication() {
		Spark.port(SERVER_PORT);

		new AlumnoController(new AlumnoRepository());
	}
}