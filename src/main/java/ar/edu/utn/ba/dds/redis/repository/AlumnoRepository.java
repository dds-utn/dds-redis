package ar.edu.utn.ba.dds.redis.repository;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ar.edu.utn.ba.dds.redis.exception.NoExisteAlumnoException;
import ar.edu.utn.ba.dds.redis.model.Alumno;

/**
 * Repositorio donde persisto los alumnos.
 *
 */
public class AlumnoRepository {

	private static final String REDIS_URL = "redis://localhost:6379";

	private static final Logger logger = LoggerFactory.getLogger(AlumnoRepository.class);

	private RedisRepository redis;

	public AlumnoRepository() {
		super();
		this.redis = new RedisRepository(REDIS_URL);
	}

	/**
	 * Persiste un alumno.
	 * 
	 * @param alumnoNuevo
	 *            a persistir.
	 * @return alumno json.
	 */
	public String save(Alumno alumnoNuevo) {
		String alumnoJSON = this.getAlumnoJSON(alumnoNuevo);
		this.redis.setInfo(alumnoNuevo.getLegajo(), alumnoJSON);
		return alumnoJSON;
	}

	/**
	 * Obtiene todos los alumnos persistidos.
	 * 
	 * @return una lista de alumnos persistidos.
	 */
	public List<Alumno> getAll() {
		List<String> alumnosJson = this.redis.getAllInfo();
		List<Alumno> alumnos = new ArrayList<>();

		alumnosJson.forEach(alumnoJSON -> {
			try {
				alumnos.add(this.getAlumnoFromJson(alumnoJSON));
			} catch (IOException e) {
				logger.error("Problema al interpretar un alumno json");
			}
		});

		return alumnos;
	}

	/**
	 * Obtiene un alumno por su legajo.
	 * 
	 * @param legajo
	 *            del alumno
	 * @return un alumno.
	 * @throws NoExisteAlumnoException
	 *             se busca un alumno que no esta persistido
	 * @throws IOException
	 *             problema al intentar obtener un alumno de un JSON
	 */
	public Alumno get(String legajo) throws NoExisteAlumnoException, IOException {
		String alumnoJSON = this.redis.getInfo(legajo);

		if (alumnoJSON != null) {
			return this.getAlumnoFromJson(alumnoJSON);
		}

		throw new NoExisteAlumnoException();
	}

	/**
	 * Obtiene un alumno basado en el json dado.
	 * 
	 * @param alumnoJson
	 *            json que contiene un alumno.
	 * @return alumno basado en el json.
	 * @throws IOException
	 *             problema al interpretar el json.
	 */
	public Alumno getAlumnoFromJson(String alumnoJson) throws IOException {
		JsonNode json = new ObjectMapper().readTree(alumnoJson);
		return new Alumno(json.get("nombre").asText(), json.get("dni").asText(), json.get("legajo").asText());
	}

	/**
	 * Obtiene un JSON que representa a un alumno.
	 * 
	 * @param alumno
	 * @return
	 */
	public String getAlumnoJSON(Alumno alumno) {
		return this.dataToJson(alumno);
	}

	/**
	 * Obtienen una String JSON con todos los alumnos.
	 * 
	 * @param alumnos
	 *            a convertir en JSON
	 * @return String JSON con todos los alumnos contenidos.
	 */
	public String getAlumnosJSON(List<Alumno> alumnos) {
		return this.dataToJson(alumnos);
	}

	/**
	 * Convierte un objeto a JSON.
	 * 
	 * @param data
	 *            objeto a convertir.
	 * @return JSON que representa al objeto.
	 */
	private String dataToJson(Object data) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			StringWriter sw = new StringWriter();
			mapper.writeValue(sw, data);
			return sw.toString();
		} catch (IOException e) {
			throw new RuntimeException("IOException from a StringWriter?");
		}
	}
}