package ar.edu.utn.ba.dds.redis.repository;

import java.util.ArrayList;
import java.util.List;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

/**
 * Repositorio que almacena los elementos en un servidor Redis.
 *
 */
public class RedisRepository {
	private RedisClient redisClient;
	private StatefulRedisConnection<String, String> connection;

	public RedisRepository(String redisUrl) {
		super();

		this.redisClient = RedisClient.create(redisUrl);
		this.connection = redisClient.connect();
	}

	/**
	 * Almacena la informacion clave/valor en Redis.
	 * 
	 * @param key
	 * @param value
	 */
	public void setInfo(String key, String value) {
		RedisCommands<String, String> syncCommands = connection.sync();
		syncCommands.set(key, value);
	}

	/**
	 * Obtiene la informacion almacenada en Redis.
	 * 
	 * @param key
	 * @return string del objeto almacenado.
	 */
	public String getInfo(String key) {
		RedisCommands<String, String> syncCommands = connection.sync();
		return syncCommands.get(key);
	}

	/**
	 * Obtiene la lista de elementos almacenados en Redis.
	 * 
	 * @return una lista de string JSON con los objetos almacenados.
	 */
	public List<String> getAllInfo() {
		RedisCommands<String, String> syncCommands = connection.sync();
		List<String> hotelJson = new ArrayList<>();
		List<String> keys = syncCommands.keys("*");
		keys.forEach(key -> hotelJson.add(syncCommands.get(key)));
		return hotelJson;
	}

	/**
	 * Close Redis connection.
	 */
	public void closeRedis() {
		this.connection.close();
		this.redisClient.shutdown();
	}
}