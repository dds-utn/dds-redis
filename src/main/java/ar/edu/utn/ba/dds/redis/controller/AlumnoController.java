package ar.edu.utn.ba.dds.redis.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ar.edu.utn.ba.dds.redis.exception.NoExisteAlumnoException;
import ar.edu.utn.ba.dds.redis.model.Alumno;
import ar.edu.utn.ba.dds.redis.repository.AlumnoRepository;
import spark.Request;
import spark.Response;
import spark.Spark;

/**
 * Controlador para los recursos de Alumno.
 * 
 * @author rricomendoza
 *
 */
public class AlumnoController {

	private static final String ALUMNOS_URL = "/alumnos";
	private static final String ALUMNOS_LEGAJO_URL = "/alumnos/:legajo";

	private static final Logger logger = LoggerFactory.getLogger(AlumnoController.class);

	private AlumnoRepository alumnoRepository;

	/**
	 * Contructor del controller
	 * 
	 * @param alumnoRepository
	 *            repositorio donde se persisten los alumnos.
	 */
	public AlumnoController(AlumnoRepository alumnoRepository) {
		this.alumnoRepository = alumnoRepository;
		Spark.get(ALUMNOS_URL, (request, response) -> getAlumnos(request, response));
		Spark.get(ALUMNOS_LEGAJO_URL, (request, response) -> getAlumno(request, response));
		Spark.post(ALUMNOS_URL, (request, response) -> postAlumno(request, response));

		logger.info("Alumno controller listo.");
	}

	/**
	 * Atiende los post de alumnos.
	 * 
	 * @param request
	 *            http request.
	 * @param response
	 *            http response.
	 * @return json con el id del alumno.
	 */
	private String postAlumno(Request request, Response response) {
		try {
			Alumno alumnoNuevo = new ObjectMapper().readValue(request.body(), Alumno.class);
			String alumno = this.alumnoRepository.save(alumnoNuevo);
			logger.info("Alumno " + alumnoNuevo.getLegajo() + " grabado");
			return alumno;
		} catch (Exception ex) {
			logger.error("Error al intentar instanciar el alumno.", ex);
			response.status(500);
			response.type("application/json");
			return "Problema al intentar crear un alumno";
		}
	}

	/**
	 * Atiende los get para los alumnos.
	 * 
	 * @param request
	 *            http request.
	 * @param response
	 *            http response.
	 * @return json con los alumnos.
	 */
	private String getAlumnos(Request request, Response response) {
		List<Alumno> alumnos = this.alumnoRepository.getAll();
		return this.alumnoRepository.getAlumnosJSON(alumnos);
	}

	/**
	 * Atiende los get para un alumno.
	 * 
	 * @param request
	 *            http request.
	 * @param response
	 *            http response.
	 * @return json con el alumno.
	 */
	private String getAlumno(Request request, Response response) {
		String legajo = request.params(":legajo");
		Alumno alumno = null;
		try {
			alumno = this.alumnoRepository.get(legajo);
		} catch (NoExisteAlumnoException | IOException e) {
			response.status(404);
			response.type("application/json");
			logger.error("Error al obtener un alumno.");
		}
		if (alumno != null) {
			return this.alumnoRepository.getAlumnoJSON(alumno);
		} else {
			response.status(404);
			return "No se encontro el alumno pedido.";
		}
	}
}