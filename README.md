# dds-redis

## Description
Proyecto que se usa en el taller de redis.

### Project dependencies
* Java 8.
* Maven.
* Redis.

### URLs
* GET: http://localhost:8080/alumnos/
* GET: http://localhost:8080/alumnos/:legajo
* POST: http://localhost:8080/alumnos
  * Payload:

    ```json
    {
	  "nombre":"Mito",
	  "dni":"123456789",
	  "legajo":"11111-1"
	}
    ```